FROM centos:7

RUN for iter in {1..3}; do \
    yum update --setopt=tsflags=nodocs -y && \
    yum install --setopt=tsflags=nodocs -y \
    gcc \
        openssl-devel \
        bzip2-devel \
        libffi-devel \
        make \
        zlib-devel \
        xz-devel \
    wget \
        && yum clean all && \
        exit_code=0 && break || exit_code=$? && echo "yum error: retry $iter in 10s" && sleep 10; \
    done;

WORKDIR /usr/src

COPY requirements.txt requirements.txt

RUN wget --no-verbose https://www.python.org/ftp/python/3.7.12/Python-3.7.12.tgz && \
        echo '6fe83678c085a7735a943cf1e4d41c14  Python-3.7.12.tgz' > Python-3.7.12.tgz.md5 && \
    md5sum --check Python-3.7.12.tgz.md5 && \
        tar xzf Python-3.7.12.tgz && \
        rm -f Python-3.7.12.tgz && \
        Python-3.7.12/configure --enable-optimizations && \
        make altinstall && \
        python3.7 -m ensurepip && \
        python3.7 -m pip install --upgrade pip && \
        python3.7 -m pip install -r requirements.txt

WORKDIR /python_api

COPY python-api.py python-api.py

ENTRYPOINT ["/usr/local/bin/python3.7","python-api.py"]
